package com.example.tarik.secondprojectfromskiff;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editText1;
    EditText editText2;
    EditText editText3;
    Button loadId;
    Button saveId;

    public String firstName;
    public String lastName;
    public String position;
    String s = new String();

    public SharedPreferences sharedPreferences1;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        loadId = (Button) findViewById(R.id.loadId);
        saveId = (Button) findViewById(R.id.saveId);

        loadId.setOnClickListener(this);
        saveId.setOnClickListener(this);

        sharedPreferences1 = getSharedPreferences(Settings.FILE_NAME, Context.MODE_PRIVATE);

    }

    @Override
    public void onClick(View view) {


        switch (view.getId()) {
            case R.id.saveId:
                if (!editText1.getText().toString().equals("")) {
                    Settings.setFirstName(editText1.getText().toString(), sharedPreferences1);
                    editText1.setText(s);
                } else {
                    Toast.makeText(MainActivity.this, "Заполните все поля ввода!", Toast.LENGTH_SHORT).show();
                }
                if (!editText2.getText().toString().equals("")) {
                    Settings.setLastName(editText2.getText().toString(), sharedPreferences1);
                    editText2.setText(s);
                } else {
                    Toast.makeText(MainActivity.this, "Заполните все поля ввода!", Toast.LENGTH_SHORT).show();
                }
                if (!editText3.getText().toString().equals("")) {
                    Settings.setPosition(editText3.getText().toString(), sharedPreferences1);
                    editText3.setText(s);
                } else {
                    Toast.makeText(MainActivity.this, "Заполните все поля ввода!", Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(MainActivity.this, "Данные сохранены!", Toast.LENGTH_SHORT).show();
                break;

        /*switch (view.getId()) {
            case R.id.saveId:
                if (!editText1.getText().toString().equals("")) {
                    firstName = editText1.getText().toString();
                    editText1.setText(s);
                } else {
                    Toast.makeText(MainActivity.this, "Заполните поле Имя:!", Toast.LENGTH_SHORT).show();
                }
                if (!editText2.getText().toString().equals("")) {
                    lastName = editText2.getText().toString();
                    editText2.setText(s);
                } else {
                    Toast.makeText(MainActivity.this, "Заполните поле Фамилия:!", Toast.LENGTH_SHORT).show();
                }
                if (!editText3.getText().toString().equals("")) {
                    position = editText3.getText().toString();
                    editText3.setText(s);
                } else {
                    Toast.makeText(MainActivity.this, "Заполните поле Должность:!", Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(MainActivity.this, "Данные сохранены!", Toast.LENGTH_SHORT).show();
                break;*/


            case R.id.loadId:
                editText1.setText(Settings.getFirstName(sharedPreferences1));
                editText2.setText(Settings.getLastName(sharedPreferences1));
                editText3.setText(Settings.getPosition(sharedPreferences1));


                /*editText1.setText(firstName);
                editText2.setText(lastName);
                editText3.setText(position);
*/

                Toast.makeText(MainActivity.this, "Данные возобновлены!", Toast.LENGTH_SHORT).show();
                break;
                //    mOwner = setValue;


        }
    }
}
